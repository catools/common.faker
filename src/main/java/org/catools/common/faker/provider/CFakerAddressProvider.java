package org.catools.common.faker.provider;

import lombok.AllArgsConstructor;
import org.catools.common.faker.model.CRandomAddress;
import org.catools.common.faker.model.CRandomCity;
import org.catools.common.faker.model.CRandomStreetInfo;
import org.catools.common.utils.CIterableUtil;

@AllArgsConstructor
public class CFakerAddressProvider {
    private final CFakerCountryProvider country;
    private final CFakerStreetAddressProvider streetAddressProvider;

    public CRandomAddress getAny() {
        return getAny(CIterableUtil.getRandom(country.getStateProviders()).getState().getCode());
    }

    public CRandomAddress getAny(String stateCode) {
        CFakerStateProvider state = country.getStateProviders().stream().filter(s -> s.getState().getCode().equalsIgnoreCase(stateCode)).findFirst().get();
        return getAny(stateCode, state.getRandomCity().getName());
    }

    public CRandomAddress getAny(String stateCode, String cityname) {
        CFakerStateProvider state = country.getStateProviders().stream().filter(s -> s.getState().getCode().equalsIgnoreCase(stateCode)).findFirst().get();
        CRandomCity city = state.getCities().stream().filter(s -> s.getName().equalsIgnoreCase(cityname)).findFirst().get();
        CRandomStreetInfo streetInfo = streetAddressProvider.getAny();
        return new CRandomAddress(country.getCountry(),
                state.getState(),
                city,
                streetInfo.getStreetName(),
                streetInfo.getStreetSuffix(),
                streetInfo.getStreetPrefix(),
                streetInfo.getStreetNumber(),
                streetInfo.getBuildingNumber());
    }
}
